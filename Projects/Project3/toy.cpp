#include <iostream>
#include "bst.h"


int main(){
    int x;
    int h;
    int z = 1;
    int b;

    bst<int> *t = new bst<int>();

    while( (std::cin >> x)) 
        t->insert(x);

    if(t->size() <= 20) {
      t->inorder();
      t->preorder();
    }
    bool test;
    //x=22;
    std::vector<int> * testorder = t->extract_range(1,4);
    //test = t->remove(x);
    h = t->height();
    for(int i = 0; i < (*testorder).size(); i++)
    {
      printf("%d",(*testorder)[i]);
    }
   /* x= 4;
    test = t->remove(x);

    x= 5;
    test = t->remove(x);

    t->inorder();
      t->preorder();
    /*z = t->num_geq(17);
    b = t->num_geq_SLOW(17);
    printf("Greater Than:  %d\n", z);
    printf("Greater Than:  %d\n", b);

    z = t->num_leq(110);
    b = t->num_leq_SLOW(110);
    printf("Less Than:  %d\n", z);
    printf("Less Than:  %d\n", b);

    z = t->num_range(11, 12);
    b = t->num_range_SLOW(11, 12);
    printf("Range:  %d\n", z);
    printf("Range:  %d\n", b);

    z = t->num_geq(11);
    b = t->num_geq_SLOW(11);
    printf("Greater Than:  %d\n", z);
    printf("Greater Than:  %d\n", b);

    z = t->num_leq(12);
    b = t->num_leq_SLOW(12);
    printf("Less Than:  %d\n", z);
    printf("Less Than:  %d", b);*/

    printf("Value: %d\n",t -> get_ith(1, z));
    printf("Value: %d\n",t -> get_ith(2, z));
    printf("Value: %d\n",t -> get_ith(3, z));
    printf("Value: %d\n",t -> get_ith(4, z));
    printf("Value: %d\n",t -> get_ith(5, z));
    printf("Value: %d\n",t -> get_ith(6, z));
    printf("Value: %d\n",t -> get_ith(7, z));
    printf("Value: %d\n",t -> get_ith(8, z));

    printf("Position: %d\n", t->position_of(1));
    printf("Position: %d\n", t->position_of(2));
    printf("Position: %d\n", t->position_of(3));
    printf("Position: %d\n", t->position_of(4));
    printf("Position: %d\n", t->position_of(5));
    //printf("Position: %d\n", t->position_of(6));
    //printf("Position: %d\n", t->position_of(7));
    //printf("Position: %d\n", t->position_of(8));
    //printf("Position: %d\n", t->position_of(10));

    //printf("X value : %d", z);
    std::cout << "\n#### Reported height of tree:   " << h << "\n";

    delete t;
    
    return 0;
}
