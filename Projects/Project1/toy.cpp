#include "TravelOptions.h"

#include <stdlib.h>
#include <iostream>

// using namespace std;

/*
 simple demo program exercising some of the TravelOptions functions.

to compile:  g++ -std=c++11 toy.cpp

 purpose:  just to make sure you have an idea of how to write 
             driver/tester programs (this is not itself a tester program
             per-se, just a pretty random sequence of calls..
*/


int main(int argc, char *argv[]){
  TravelOptions *options_p, *options_p2, *options_p3;

  std::vector<std::pair<double, double>>
	vec{ {0, 4},{1, 3 }, {2,2}, {3,1}, {4, 0}};
  std::vector<std::pair<double, double>>
	vec3{ {4,8},{5,7}, {6,6}, {7,5}};

  std::vector<std::pair<double, double>> *vec2;
  std::vector<std::pair<double, double>> *vec4;
  //remember:  from_vec is a static function (hence, TravelOptions:: form)
  options_p = TravelOptions::from_vec(vec);
  options_p2 = TravelOptions::from_vec(vec3);

  options_p->display();
  std::cout << "size: " << options_p->size() << std::endl; 
 
  TravelOptions::Relationship r;
  r = TravelOptions::compare(1, 6, 1, 7);
   if(r == TravelOptions::incomparable) 
     std::cout << "INCOMPARABLE!" << std::endl;
   if(r == TravelOptions::better) 
     std::cout << "Better" << std::endl;
   if(r == TravelOptions::worse) 
     std::cout << "worse" << std::endl;
   if(r == TravelOptions::equal) 
     std::cout << "equal" << std::endl;

  vec2 = options_p->to_vec();
  vec4 = options_p2->to_vec();
  std::cout << "CONTENTS OF EXPORTED VECTOR 1:\n";
  for(int i=0; i<vec2->size(); i++) {
     std::cout << (*vec2)[i].first << (*vec2)[i].second << std::endl;
  }
  std::cout << "CONTENTS OF EXPORTED VECTOR 2:\n";
  for(int i=0; i<vec2->size(); i++) {
     std::cout << (*vec4)[i].first << (*vec4)[i].second << std::endl;
  }
  std::cout << "\n";

  if(options_p->is_sorted() ) 
     std::cout << "SORTED!" << std::endl;

   if( options_p->is_pareto() )
     std::cout << "PARETO!" << std::endl;
   else
     std::cout << "NOT PARETO!" << std::endl;
   if(options_p->is_pareto_sorted())
     std::cout << "PARETO IS SORTED!" << std::endl;
   else
     std::cout << "NOT PARETO!" << std::endl;
   /*if(options_p->insert_sorted(6, 3))
     std::cout << "INSERT_SORTED WORKED!" << std::endl;
   if(options_p->insert_sorted(3, 2))
     std::cout << "INSERT_SORTED WORKED!" << std::endl;
   if(options_p->insert_sorted(2, 7))
     std::cout << "INSERT_SORTED WORKED!" << std::endl;
   options_p->display();*/
   //if(options_p->insert_pareto_sorted(3, 6))
     //std::cout << "INSERT_PARETO_SORTED WORKED!" << std::endl;
   //options_p->display();
   //options_p3= options_p->union_pareto_sorted(*options_p2);
   //options_p3->display();
   //if(options_p->prune_sorted())
     //std::cout << "prune_sorted worked!" << std::endl;
      //options_p->display();
  //delete options_p3;

  //options_p3 = options_p->join_plus_plus(*options_p2);
  //options_p3->display();
  //delete options_p3;
  options_p3 = options_p->join_plus_max(*options_p2);
  options_p3->display();
  //options_p3 = options_p-> split_sorted_pareto(0);
  //options_p->display();
  //options_p3->display();
  /*options_p->clear();
  delete options_p;
  delete options_p2;
  delete options_p3;*/

  return 0;
}
