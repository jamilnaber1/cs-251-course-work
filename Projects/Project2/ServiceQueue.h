/* Name: Program 2 (Service Queue)
 * Class: CS251, 2pm, T and Tr
 * Autor: Jamil Naber (jnaber3)
 * System: systems3, c11
 * Purpose: This file stores all the functions and data members for class ServiceQueue.h.
 * 					The status of each function, with its run time, is below:
 * 					ServiceQueue():  Done , constant
 * 					~ServiceQueue(): Done , linear
 * 					pushFront():     Done , constant*     *Helper
 * 					pushBack():      Done , constant*     *Helper
 * 					pop();           Done , constant*     *Helper
 * 					snapshot():      Done , linear
 *  				length():        Done , constant
 * 					give_buzzer():   Done , constant*     *AMORTIZED
 * 					seat():          Done , constant
 * 					kick_out():      Done , constant
 * 					take_bribe():    Done , constant
*/
#ifndef _SERVICE_QUEUE_H
#define _SERVICE_QUEUE_H

#include <iostream>
#include <vector>
#include <utility>



class ServiceQueue {

  private:
        /** Your private data members here! 
        *   (you should have NO PUBLIC data members! 
        *                                            
        * Nested private types also go here.  
        * For example, if your design needs some kind of
        *   structure, you would specify it here.
        */ 
	   struct Node                     //Structure Node is created, it is doublely linked, a data member of the class 
	   {
		   int buzzer_id;                //data memeber of Node, buzzer_id, holds the buzzer id
		   Node* next;                   //data memeber of Node, next, holds the pointer to the next node
		   Node* before;                 //data memeber of Node, before, holds the pointer to the node before
		   Node(int _buzzer_id = -1, Node* _next = nullptr, Node* _before = nullptr)  //Constructor for sturcture of Node w/ defaults
		   {
			   buzzer_id = _buzzer_id;                         
			   next = _next;
			   before = _before;
		   }
	   };

	   std::vector<Node*> buzzer_2_info;   //vector that holds pointers to Node's
	   int size = 0;                       //size to keep track of uniqe id's
		 int currentSize = 0;                //holds the current lenght of the list
	   Node* frontServiceQueue;            //front pointer to the ServiceQueue
	   Node* backServiceQueue;             //back pointer to the ServiceQueue
	   Node* frontBuzzerBucket;            //front pointer to the frontBuzzerBucket
	   Node* backBuzzerBucket;             //back pointer to the frontBuzzerBucket
  public:

	/**
	 * Constructor
	 * Description: intializes an empty service queue.
	 * 
	 * RUNTIME REQUIREMENT: O(1)
	 *
         * TODO
	 */
	ServiceQueue() 
	{
		//set all front and back pointers to NULL
		frontServiceQueue = nullptr;
		backServiceQueue = nullptr;
		frontBuzzerBucket = nullptr;
		backBuzzerBucket = nullptr;
	}

	/**
	 * Destructor
	 * Description:  deallocates all memory assciated
	 *   with service queue 
	 *
	 * RUNTIME REQUIREMENT:  O(N_b) where N_b is the number of buzzer 
	 *	IDs that have been used during the lifetime of the
	 *	service queue; in general, at any particular instant
	 *	the actual queue length may be less than N_b.
	 *
	 *	[See discussion of "re-using buzzers" below]
	 *
         * TODO
	 */
	~ServiceQueue() {
		Node* tmp = NULL;                              //Have a Node pointer type, tmp, to hold node pointers
		while(frontServiceQueue != NULL)               //While loop will run till frontServiceQueue has no more Nodes to point too
		{
			tmp = frontServiceQueue;                     //Have tmp hold the front
			frontServiceQueue = frontServiceQueue->next; //move the fornt to the next node
			delete tmp;                                  //delete the old front
		}
		while(frontBuzzerBucket != NULL)               //While loop will run till frontServiceQueue has no more Nodes to point too
		{
			tmp = frontBuzzerBucket;                     //Have tmp hold the front
			frontBuzzerBucket = frontBuzzerBucket->next; //move the fornt to the next node
			delete tmp;                                  //delete the old front
		}
  }


/**
 *  pushFront
 *  Description: Pushes values to the front of the list
 *  It takes two Node pointers, front and back pass-by-refrence. 
 *  Runtime: O(1)
 * 
 */
	void pushFront(Node** queue, Node** bqueue, int value)
	{
		Node* tmp = new Node(value);   //create a new Node that has the new value
		if(*queue == NULL)             //check if the list is empty
		{
			*queue = tmp;                //Have tmp be a the front
			*bqueue = tmp;               //also back
		}
		else
		{
			tmp -> next = (*queue);     //tmp next will be front
			(*queue) -> before = tmp;   //fronts before will be tmp
			(*queue) = tmp;             //tmp is the new front
		}
	}//end of pushFront()


/**
 *  pushBack
 *  Description: Pushes values to the back of the list
 *  It takes two Node pointers, front and back pass-by-refrence. 
 *  Runtime: O(1)
 * 
 */
	void pushBack(Node** queue, Node** bqueue, int value)
	{
		Node* tmp = new Node(value); //create a new Node that has the new value
		if(*queue == NULL)           //check if the list is empty
		{
			*queue = tmp;              //Have tmp be a the front
			*bqueue = tmp;             //also back
		}
		else
		{
			tmp -> before = (*bqueue); //tmp before will be back
			(*bqueue) -> next = tmp;   //back next will be tmp
			(*bqueue) = tmp;           //tmp is the new back
		}
	}//end of pushBack()


/**
 *  pop
 *  Description: pops values to the front of the list
 *  It takes two Node pointers, front and back pass-by-refrence.
 * 	It has a return type of integer 
 *  Runtime: O(1)
 * 
 */
	int pop(Node** queue, Node** bqueue)
	{
		Node* tmp = NULL;                //Declare Node pointer tmp
		int value;                       //Declare integer value
		if((*queue) == (*bqueue))        //Check if the list has one node
		{
			value = (*queue) -> buzzer_id; //get the value at the front of the list
			delete *queue;                 //delete the fornt of the lsit
			(*queue) = NULL;               //set front to NULL
			return value;                  //return the value that was popped
		}
		//If there is more than one Node
		else
		{
			tmp = (*queue);                //have tmp hold the front
			value = (*queue) -> buzzer_id; //get the value at the front
			(*queue) = (*queue) -> next;   //move the front to the next Node
			delete tmp;                    //delete the old front
			return value;                  //return the popped value
		}
	}//end of pop()


	/**
	 * Function: snapshot()
         * param:  buzzers is an integer vector passed by ref
	 * Description:  populates buzzers vector with a "snapshot"
         *               of the queue as a sequence of buzzer IDs 
         *
	 *
	 * RUNTIME REQUIREMENT:  O(N)  (where N is the current queue
	 *		length).
	 */
	void snapshot(std::vector<int> &buzzers) 
	{
    buzzers.clear();   // you don't know the history of the 
                       //   buzzers vector, so we had better
                       //   clear it first.
		Node* queue = frontServiceQueue; //have a value pointer to the fornt
		while (queue != NULL)                  //check if queue is not NULL
		{
			buzzers.push_back(queue->buzzer_id); //push the value at queue to the back of the vector
			queue = queue -> next;               //move queue to the next node
		}//end of loop
  }//end of snapshot()


	/**
	 * Function: length()
	 * Description:  returns the current number of
	 *    entries in the queue.
	 *
	 * RUNTIME REQUIREMENT:  O(1)
	 */
	int  length() {

		return currentSize;   // CurrentSize will hold the lenght

	}

	/**
	 * Function: give_buzzer()
         * Return:   buzzer-ID (integer) assigned to the new customer.
	 * Description:  This is the "enqueue" operation.  For us
	 *    a "buzzer" is represented by an integer (starting
	 *    from zero).  The function selects an available buzzer 
	 *    and places a new entry at the end of the service queue 
	 *    with the selected buzer-ID. 
	 *    This buzzer ID is returned.
	 *    The assigned buzzer-ID is a non-negative integer 
	 *    with the following properties:
	 *
	 *       (1) the buzzer (really it's ID) is not currently 
	 *         taken -- i.e., not in the queue.  (It
	 *         may have been in the queue at some previous
	 *         time -- i.e., buzzer can be re-used).
	 *	  This makes sense:  you can't give the same
	 *	  buzzer to two people!
	 *
	 *       (2) Reusable Buzzers:  A re-usable buzzer is 
	 *	  a buzzer that _was_ in the queue at some previous
	 *	  time, but currently is not.
	 *
         *         REQUIREMENT:  If there is one or more reusable
         *         buzzer, you MUST return one of them; furthermore,
         *         it must be the buzzer that became reusable most 
         *         MOST RECENTLY.
	 *
	 *       (3) if there are no previously-used / reusable buzzers, 
         *         the SMALLEST possible buzzer-ID is used (retrieved from 
         *         inventory).  
	 *	    Properties in this situation (where N is the current
	 *	      queue length):
	 *
	 *		- The largest buzzer-ID used so far is N-1
	 *
	 *		- All buzzer-IDs in {0..N-1} are in the queue
	 *			(in some order).
	 *
	 *		- The next buzzer-ID (from the basement) is N.
	 *
	 *    In other words, you can always get more buzzers (from
	 *    the basement or something), but you don't fetch an
	 *    additional buzzer unless you have to (i.e., no reusable buzzers).
	 *
	 * Comments/Reminders:
	 *
	 *	Rule (3) implies that when we start from an empty queue,
	 *	the first buzzer-ID will be 0 (zero).
	 *
	 *	Rule (2) does NOT require that the _minimum_ reuseable 
	 *	buzzer-ID be used.  If there are multiple reuseable buzzers, 
	 *	any one of them will do.
	 *	
	 *	Note the following property:  if there are no re-useable 
	 *	buzzers, the queue contains all buzzers in {0..N-1} where
	 *       N is the current queue length (of course, the buzzer IDs 
	 *	may be in any order.)
	 *
	 * RUNTIME REQUIREMENT:  O(1)  ON AVERAGE or "AMORTIZED"  
	 *          In other words, if there have been M calls to 
	 *		give_buzzer, the total time taken for those 
	 *		M calls is O(M).
	 *
	 *		An individual call may therefore not be O(1) so long
	 *		as when taken as a whole they average constant time.
	 *
	 */
	int  give_buzzer() 
	{	
		int value = size;                                         //set value to size, will change if nessasey 
		if(frontBuzzerBucket != NULL)                             //If there is buzzers in the buzz bucket
		{
			value = pop(&frontBuzzerBucket, &backBuzzerBucket);    //call pop, and have the return of pop be value
			pushBack(&frontServiceQueue,&backServiceQueue,value);  //call pushBack, this will put value, the buzzer into the queue
			buzzer_2_info.at(value) = backServiceQueue;            //have the pointer of where the value on the list will be stored
			currentSize = currentSize + 1;                         //increse the lenght
			return value;                                          //return the buzzer id
		}
		pushBack(&frontServiceQueue,&backServiceQueue,value);   //call pushBack, this will put value, the buzzer into the queue
		buzzer_2_info.push_back(backServiceQueue);              //call pushBack, this will put value, the buzzer into the queue
		size = size + 1;                                        //increse the size so buzzer does not get repeated
		currentSize = currentSize + 1;                          //increse the lenght
    return value;  // placeholder                           //return the buzzer id
  }//end of give_buzzer()

	/**
	 * function: seat()
	 * description:  if the queue is non-empty, it removes the first 
	 *	 entry from (front of queue) and returns the 
	 *	 buzzer ID.
	 *	 Note that the returned buzzer can now be re-used.
	 *
	 *	 If the queue is empty (nobody to seat), -1 is returned to
	 *	 indicate this fact.
         *
         * Returns:  buzzer ID of dequeued customer, or -1 if queue is empty.
	 *
	 * RUNTIME REQUIREMENT:  O(1)
	 */
	int seat() {
		int buzzerPop;                                                  //declare integer holder
		if(frontServiceQueue == NULL)                                   //check if the queue is empty
		{
			return -1;  //return false
		}
		else
		{
			buzzerPop = pop(&frontServiceQueue, &backServiceQueue);      //call pop and return the buzzer that was pop and store it in buzzerpop
			buzzer_2_info.at(buzzerPop) = NULL;                          //set the Node pointer vector at that buzzer to NULL
			pushFront(&frontBuzzerBucket, &backBuzzerBucket, buzzerPop); //push the popped buzzer into the buzzer bucket
			currentSize = currentSize - 1;                               //decerment the current size (lenght)
			return buzzerPop;                                            //retrun the popped buzzer id
		} 
	}//end of seat()


	/**
	 * function: kick_out()
	 *
	 * description:  Some times buzzer holders cause trouble and
	 *		a bouncer needs to take back their buzzer and
	 *		tell them to get lost.
	 *
	 *		Specifially:
	 *
	 *		If the buzzer given by the 2nd parameter is 
	 *		in the queue, the buzzer is removed (and the
	 *		buzzer can now be re-used) and 1 (one) is
	 *		returned (indicating success).
	 *
	 * Return:  If the buzzer isn't actually currently in the
	 *		queue, the queue is unchanged and false is returned
	 *		(indicating failure).  Otherwise, true is returned.
	 *
	 * RUNTIME REQUIREMENT:  O(1)
	 */
	bool kick_out(int buzzer) {
		Node* ntmp = NULL;                                           //Node pointer set to NULL, will hold next values
		Node* btmp = NULL;                                           //Node pointer set to NULL, will hold before values
		if( buzzer >= size  || buzzer_2_info.at(buzzer) == NULL)     //check if the buzzer is in the queue
		{
			return false;  // return false if not
		}
		Node** kick = &buzzer_2_info.at(buzzer);                     //Get the refrenace to the pointer Node at the buzzer id
		if(frontServiceQueue == backServiceQueue)                    //check if it is the only node in the queue
		{
			delete frontServiceQueue;                                  //delete the front Node
			kick == NULL;                                              //set the value in the vector to NULL
		}
		else if((*kick) == frontServiceQueue)                        //check if the buzzer is in the front of the queue
		{
			frontServiceQueue = (*kick)-> next;                        //set the new front past the node to be kicked
			delete (*kick);                                            //delete the old front
			frontServiceQueue -> before = NULL;                        //set the new fronts before to NULL 
		}
		else if((*kick) == backServiceQueue)                         //check if the buzzer is in the back of the queue
		{
			backServiceQueue = (*kick) -> before;                      //set the new back before the node to be kicked
			delete (*kick);                                            //delete the old back
			backServiceQueue -> next = NULL;                           //set the new back next to NULL 
		}
		else                                                         //if the node is in the middle
		{
			ntmp = (*kick) -> next;                                    //set ntmp to hold the node after kick
			btmp = (*kick)-> before;                                   //set btmp to hold the node before kick
			btmp -> next = ntmp;                                       //set the next node in the before to the next node of kick
			ntmp -> before = btmp;                                     //now the back
			delete (*kick);                                            //delete the kick Node
		}
		buzzer_2_info.at(buzzer) = NULL;
		pushFront(&frontBuzzerBucket, &backBuzzerBucket, buzzer);    //push the buzzer to the buzzer bucket
		currentSize = currentSize - 1;                               //incerment the currentSize (lenght)
		return true;                                                 //return true;
	}

	/**
	 * function:  take_bribe()
	 * description:  some people just don't think the rules of everyday
	 *		life apply to them!  They always want to be at
	 *		the front of the line and don't mind bribing
	 *		a bouncer to get there.
	 *
	 *	        In terms of the function:
	 *
	 *		  - if the given buzzer is in the queue, it is 
	 *		    moved from its current position to the front
	 *		    of the queue.  1 is returned indicating success
	 *		    of the operation.
	 *		  - if the buzzer is not in the queue, the queue 
	 *		    is unchanged and 0 is returned (operation failed).
	 *
	 * Return:  If the buzzer isn't actually currently in the
	 *		queue, the queue is unchanged and false is returned
	 *		(indicating failure).  Otherwise, true is returned.
	 *
	 * RUNTIME REQUIREMENT:  O(1)
	 */
	bool take_bribe(int buzzer) {
		Node* ntmp = NULL;                                        //Node pointer set to NULL, will hold next values
		Node* btmp = NULL;                                        //Node pointer set to NULL, will hold before values
		if( buzzer >= size  || buzzer_2_info.at(buzzer) == NULL)  //check if the buzzer is in the queue
		{
			return false;  // return false if not
		}
		Node** bribe = &buzzer_2_info.at(buzzer);                  //Get the refrenace to the pointer Node at the buzzer id
		if(frontServiceQueue == backServiceQueue)                  //check if it is the only node in the queue
		{
			return true;                                             //Do nothing, return true
		}
		else if((*bribe) == frontServiceQueue)                     //check if the Node is in the front
		{
			return true;                                             //Do nothing, return true
		}
		else if((*bribe) == backServiceQueue)                      //check if the Node is at the back
		{
			backServiceQueue = (*bribe) -> before;                  //set the back of the queue before the bribe Node
			backServiceQueue -> next = NULL;                        //set its next to NULL
		}
		else                                                      //if the bribe Node is in the middle
		{
			ntmp = (*bribe) -> next;                                //set ntmp to hold the node after bribe
			btmp = (*bribe) -> before;                              //set btmp to hold the node before bribe
			btmp -> next = ntmp;                                    //set the next node in the before to the next node of bribe
			ntmp -> before = btmp;                                  //now the back
		}
		(*bribe) -> before = NULL;                                //set bribe Nodes before to NULL
		(*bribe) -> next = frontServiceQueue;                     //set bribe Nodes next to the front of the queue
		frontServiceQueue -> before = (*bribe);                   //set the front of the list before to bribe
		frontServiceQueue = (*bribe);                             //Make bribe the new front of the list
		buzzer_2_info.at(buzzer) = frontServiceQueue;             //update Node location for the brible node
		return 1;                                                 //return true
	}//end of take_bribe()
};   // end ServiceQueue class

#endif
